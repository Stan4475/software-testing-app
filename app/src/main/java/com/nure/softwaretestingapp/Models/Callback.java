package com.nure.softwaretestingapp.Models;

/**
 * Created by Стас on 22.04.2016.
 */
public interface Callback<T,V> {
    public void onSuccess(T result);
    public void onFail(V result);
}
