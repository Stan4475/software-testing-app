package com.nure.softwaretestingapp.Models;

import com.quickblox.customobjects.model.QBCustomObject;

import java.util.Date;

/**
 * Created by Стас on 22.04.2016.
 */
public class Order {
    private Integer ticketCount;
    private String userEmail;
    String placeId;
    String placeName;
    String eventName;


    public Order(Integer ticketCount, String userEmail, String placeId, String placeName, String eventName) {
        this.ticketCount = ticketCount;
        this.userEmail = userEmail;
        this.placeId = placeId;
        this.placeName = placeName;
        this.eventName = eventName;
    }

    public QBCustomObject getQBObject(){
        QBCustomObject object = new QBCustomObject();
        object.putString("event_name", eventName);
        object.putString("place_id", placeId);
        object.putString("place_name", placeName);
        object.putString("customer_email", userEmail);
        object.putInteger("tickets_count", ticketCount);
        //object.putString("User ID", "4068367");
        object.setClassName("Order");
        return object;
    }

    public Integer getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(Integer ticketCount) {
        this.ticketCount = ticketCount;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
