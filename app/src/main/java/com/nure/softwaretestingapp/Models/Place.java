package com.nure.softwaretestingapp.Models;

import com.google.android.gms.maps.model.LatLng;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Стас on 22.04.2016.
 */
public class Place implements Comparable<String>{
    private LatLng location;
    private String name;
    private String placeId;

    public static final String QB_NAME = "Place";
    public static final String QB_LOCATION_NAME = "Location";
    public static final String QB_NAME_FIELD = "Name";

    public Place(LatLng location, String name, String placeId) {
        this.location = location;
        this.name = name;
        this.placeId = placeId;
    }

    public Place(QBCustomObject customObject) {
        List<Double> qbLoc = getLocation(customObject);
        if(qbLoc==null)
            return;
        location = new LatLng(qbLoc.get(0),qbLoc.get(1));
        name = customObject.getString(QB_NAME_FIELD);
        placeId = customObject.getCustomObjectId();
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    private List<Double> getLocation(QBCustomObject customObject) throws NumberFormatException {
        Object valueOrNull = customObject.getFields().get(QB_LOCATION_NAME);
        if(valueOrNull == null) {
            return null;
        } else {
//            if(valueOrNull instanceof ArrayList)
//                return (ArrayList) valueOrNull;
            String value = valueOrNull.toString().replaceAll("\\[", "").replaceAll("\\]","");
            String[] locationPoints = value.split(",");

            if(locationPoints.length == 2) {
                ArrayList locationValue = new ArrayList();
                locationValue.add(Double.valueOf(Double.parseDouble(locationPoints[0])));
                locationValue.add(Double.valueOf(Double.parseDouble(locationPoints[1])));
                return locationValue;
            } else {
                return null;
            }
        }
    }

    static public ArrayList<Place> createPlaceArray(ArrayList<QBCustomObject> qbObjects){
        ArrayList<Place> result = new ArrayList<>();
        for(QBCustomObject qbo : qbObjects) {
            result.add(new Place(qbo));
        }
        return result;
    }

    public Place(String name, double lat, double lng, String placeId) {
        this.name = name;
        location = new LatLng(lat, lng);
        this.placeId = placeId;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(String s) {
        if(getPlaceId() != null && s != null){
            return getPlaceId().compareTo(s);
        }
        return -1;
    }
}
