package com.nure.softwaretestingapp.Models;

import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Стас on 22.04.2016.
 */
public class Event {
    private Date date =null;
    private String name;
    private String placeId;
    private boolean isEveryWeek;

    public static final String QB_OBJECT_NAME = "Event";
    public static final String QB_DATE_FIELD_NAME = "date";
    public static final String QB_NAME_FIELD_NAME = "name";
    public static final String QB_PLACE_FIELD_NAME = "place_id";
    public static final String QB_EACH_WEEK_FIELD_NAME = "each_week";
    public Event(Date date, String name, String placeId, boolean isEveryWeek) {
        this.date = date;
        this.name = name;
        this.placeId = placeId;
        this.isEveryWeek = isEveryWeek;
    }

    public Event(QBCustomObject qbo) throws Exception{
        try {
            long longDate = qbo.getInteger(QB_DATE_FIELD_NAME).longValue();
            date = new Date(longDate*1000);
            GregorianCalendar gcDate = new GregorianCalendar();

            gcDate.setTime(new Date());
            GregorianCalendar gcEvDate = new GregorianCalendar();
            gcEvDate.setTime(date);
            gcDate.set(Calendar.HOUR_OF_DAY,gcEvDate.get(Calendar.HOUR_OF_DAY));
            gcDate.set(Calendar.MINUTE, gcEvDate.get(Calendar.MINUTE));
            while( gcDate.get( Calendar.DAY_OF_WEEK ) != gcEvDate.get(Calendar.DAY_OF_WEEK) )
                gcDate.add( Calendar.DATE, 1 );
            date = gcDate.getTime();
        } catch (Exception e) {
            throw e;
        }
        name = qbo.getString(QB_NAME_FIELD_NAME);
        placeId = qbo.getString(QB_PLACE_FIELD_NAME);
        isEveryWeek = qbo.getBoolean(QB_EACH_WEEK_FIELD_NAME);
    }

    static public ArrayList<Event> createEventArray(ArrayList<QBCustomObject> qbObjects){
        ArrayList<Event> result = new ArrayList<>();
        for(QBCustomObject qbo : qbObjects) {
            try {
                Event newEv = new Event(qbo);
                result.add(newEv);
            } catch (Exception e) {
                continue;
            }
        }
        return result;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
