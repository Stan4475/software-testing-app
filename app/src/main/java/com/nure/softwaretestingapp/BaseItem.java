package com.nure.softwaretestingapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Stan on 03.05.2016.
 */
public abstract class BaseItem {
    public abstract boolean isEventItem();

    public abstract View getView(Context mContext, ViewGroup parrent);

    public abstract View update(View convertView, Context mContext, ViewGroup parent);
}
