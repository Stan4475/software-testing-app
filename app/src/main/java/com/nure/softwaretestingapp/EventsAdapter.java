package com.nure.softwaretestingapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nure.softwaretestingapp.Models.Event;
import com.nure.softwaretestingapp.QuickBlox.DateItem;
import com.nure.softwaretestingapp.QuickBlox.EventItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Stan on 03.05.2016.
 */
public class EventsAdapter extends BaseAdapter {
    ArrayList<BaseItem> items;
    Context mContext;

    public EventsAdapter(ArrayList<Event> items, Context mContext) {
        this.items = createItemsList(items);
        this.mContext = mContext;
    }

    private ArrayList<BaseItem> createItemsList(ArrayList<Event> eItems) {
        Collections.sort(eItems, new Comparator<Event>() {
            @Override
            public int compare(Event event, Event t1) {
                return event.getDate().compareTo(t1.getDate());
            }
        });
        ArrayList<BaseItem> result =new ArrayList<>();
        Date lastDate = eItems.get(0).getDate();
        result.add(new DateItem(lastDate));
        for (Event e: eItems){
            if(e.getDate().getDay() != lastDate.getDay()){
                lastDate = e.getDate();
                result.add(new DateItem(lastDate));
            }
            result.add(new EventItem(e));
        }

        return result;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null)
            convertView = items.get(position).getView(mContext,viewGroup);
        return items.get(position).update(convertView, mContext, viewGroup);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (items.get(position).isEventItem())?1:0;
    }
}
