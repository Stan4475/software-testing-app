package com.nure.softwaretestingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.identity.intents.AddressConstants;
import com.nure.softwaretestingapp.Models.Callback;
import com.nure.softwaretestingapp.Models.Event;
import com.nure.softwaretestingapp.Models.Order;
import com.nure.softwaretestingapp.QuickBlox.QuickBloxService;

import java.util.ArrayList;

/**
 * Created by Stan on 03.05.2016.
 */
public class EventListActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {
    ListView eventList;
    TextView noEventsView;
    String placeId;
    String placeTitle;
    ProgressDialog progressDialog;
    ArrayList<Event> events;
    private static final int RC_SIGN_IN = 9001;

    private Event clickedEvent = null;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_list_activity);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        eventList = (ListView) findViewById(R.id.event_list);
        noEventsView = (TextView) findViewById(R.id.no_events);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            placeId = extras.getString(MapsActivity.PLACE_EXTRAS_ID);
            if (placeId == null) {
                noEventsView.setVisibility(View.VISIBLE);
                return;
            }
            placeTitle = extras.getString(MapsActivity.PLACE_EXTRAS_NAME);
            progressDialog.setMessage("Loading "+ placeTitle +" events");
        }
        else{
            noEventsView.setVisibility(View.VISIBLE);
            return;
        }
        progressDialog.show();
        QuickBloxService.getInstance(this).getEvents(placeId, new Callback<ArrayList<Event>, String>() {
            @Override
            public void onSuccess(ArrayList<Event> result) {
                if(result != null && result.size() > 0) {
                    events = result;
                    eventList.setAdapter(new EventsAdapter(events, EventListActivity.this));
                }
                else{
                    noEventsView.setVisibility(View.VISIBLE);
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFail(String result) {
                Toast.makeText(EventListActivity.this, "Error: " + result, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }

    public void buyTicket(Event e){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        clickedEvent = e;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            handleSignInResult(result);
            getDrawable(R.drawable.nure_logo);
        }
    }

    private void handleSignInResult(final GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final Dialog d = new Dialog(this);
            d.setTitle("Select count of tickets       ");
            d.setContentView(R.layout.activity_order_request);
            Button b1 = (Button) d.findViewById(R.id.cancel);
            Button b2 = (Button) d.findViewById(R.id.confirm);
            TextView tv = (TextView) d.findViewById(R.id.event_name);
            tv.setText(clickedEvent.getName());
            final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
            np.setMaxValue(100);
            np.setMinValue(1);
            np.setWrapSelectorWheel(false);
            b1.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Order order = new Order(np.getValue(), result.getSignInAccount().getEmail(),
                            placeId, placeTitle, clickedEvent.getName());
                    QuickBloxService.getInstance(EventListActivity.this).createOrder(order, new Callback<Void, String>() {
                        @Override
                        public void onSuccess(Void result) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(EventListActivity.this);
                            builder.setTitle("Message")
                                    .setMessage("Order successfully added")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                        @Override
                        public void onFail(String result) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(EventListActivity.this);
                            builder.setTitle("Request failed")
                                    .setMessage(result)
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                    d.dismiss();
                }
            });
            b2.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    progressDialog.dismiss();
                    d.dismiss();
                }
            });
            d.show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(EventListActivity.this);
            builder.setTitle("Request failed")
                    .setMessage(result.getStatus().getStatusMessage())
                    .setCancelable(false)
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,"Connection failed. " + connectionResult.getErrorMessage(), Toast.LENGTH_LONG).show();
    }
}
