package com.nure.softwaretestingapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nure.softwaretestingapp.Models.Callback;
import com.nure.softwaretestingapp.Models.Place;
import com.nure.softwaretestingapp.QuickBlox.QuickBloxService;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    public static final String PLACE_EXTRAS_NAME = "place_name";
    public static final String PLACE_EXTRAS_ID = "place_id";
    private GoogleMap mMap;
    View splash;
    ArrayList<Place> places;
    Button onList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        splash = findViewById(R.id.splash);

        onList = (Button) findViewById(R.id.on_list);
        onList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, PlaceListActivity.class);
                startActivity(intent);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        QuickBloxService.getInstance(this).getPlaces(new Callback<ArrayList<Place>, String>() {

            @Override
            public void onSuccess(ArrayList<Place> result) {
                places = result;
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(MapsActivity.this);

            }

            @Override
            public void onFail(String result) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                    builder.setTitle("Request failed")
                            .setMessage(result)
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            MapsActivity.this.finish();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        ((LocationManager)MapsActivity.this.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        LatLng nure = new LatLng(50.0149236,36.2258975);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(nure, 11.5f));
            if(places != null){
                for(Place p : places){
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(
                            p.getLocation().latitude, p.getLocation().longitude)).icon(
                            BitmapDescriptorFactory.fromResource(R.drawable.map_marker_other)).title(p.getName())
                    .snippet(p.getPlaceId()));
                }
            }
        } else {

            mMap.addMarker(new MarkerOptions().position(nure).title("KNURE"));

        }

        splash.setVisibility(View.INVISIBLE);
        onList.setVisibility(View.VISIBLE);

        // Add a marker in Sydney and move the camera

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        Toast.makeText(this, marker.getTitle(),Toast.LENGTH_LONG).show();
        Place clickedPl = null;
        for(Place p : places) {
            if(p.getPlaceId().equals(marker.getSnippet())){
                clickedPl = p;
                break;
            }
        }
        if (clickedPl != null) {
            Intent eventListIntent = new Intent(this, EventListActivity.class);
            eventListIntent.putExtra(PLACE_EXTRAS_NAME, clickedPl.getName());
            eventListIntent.putExtra(PLACE_EXTRAS_ID, clickedPl.getPlaceId());
            startActivity(eventListIntent);
        }
        return true;
    }

}
