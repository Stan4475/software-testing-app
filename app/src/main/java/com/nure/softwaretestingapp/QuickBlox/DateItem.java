package com.nure.softwaretestingapp.QuickBlox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nure.softwaretestingapp.BaseItem;
import com.nure.softwaretestingapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Stan on 03.05.2016.
 */
public class DateItem extends BaseItem{
    Date date;
    TextView dateView;
    public DateItem(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean isEventItem() {
        return false;
    }

    @Override
    public View getView(Context mContext, ViewGroup parent) {

        View result = LayoutInflater.from(mContext).inflate(R.layout.date_separator, parent, false);
        dateView = (TextView) result.findViewById(R.id.event_date);
        return update(result,mContext,parent);
    }

    @Override
    public View update(View convertView, Context mContext, ViewGroup parent) {
        if(! (convertView instanceof RelativeLayout)){
            convertView = getView(mContext, parent);
        }
        SimpleDateFormat format = new SimpleDateFormat("EEEE d.MM.yyyy");
        dateView.setText(format.format(date));
        return convertView;
    }
}
