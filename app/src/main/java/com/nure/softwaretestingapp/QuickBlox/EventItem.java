package com.nure.softwaretestingapp.QuickBlox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nure.softwaretestingapp.BaseItem;
import com.nure.softwaretestingapp.EventListActivity;
import com.nure.softwaretestingapp.Models.Event;
import com.nure.softwaretestingapp.R;

import java.text.SimpleDateFormat;

/**
 * Created by Stan on 03.05.2016.
 */
public class EventItem extends BaseItem {

    Event event;

    TextView time;
    TextView eventName;
    Button buy;

    public EventItem(Event event) {
        this.event = event;
    }

    @Override
    public boolean isEventItem() {
        return true;
    }

    @Override
    public View getView(Context mContext, ViewGroup parrent) {
        View result = LayoutInflater.from(mContext).inflate(R.layout.event_item, parrent, false);
        time = (TextView) result.findViewById(R.id.event_time);
        eventName = (TextView) result.findViewById(R.id.eventName);
        buy = (Button) result.findViewById(R.id.buy_ticket);
        return update(result,mContext,parrent);
    }

    @Override
    public View update(View convertView, final Context mContext, ViewGroup parent) {
        if(! (convertView instanceof LinearLayout)){
            convertView = getView(mContext, parent);
        }
        SimpleDateFormat format = new SimpleDateFormat("kk:mm");
        time.setText(format.format(event.getDate()));
        eventName.setText(event.getName());
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EventListActivity)mContext).buyTicket(event);
            }
        });
        return convertView;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
