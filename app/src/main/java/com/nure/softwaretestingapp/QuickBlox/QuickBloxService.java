package com.nure.softwaretestingapp.QuickBlox;

import android.content.Context;
import android.os.Bundle;

import com.nure.softwaretestingapp.Models.Callback;
import com.nure.softwaretestingapp.Models.Event;
import com.nure.softwaretestingapp.Models.Order;
import com.nure.softwaretestingapp.Models.Place;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Стас on 22.04.2016.
 */
public class QuickBloxService {
    private static int twoH = 7200000;

    private static QuickBloxService mInstance;
    private static Context mContext;

    private static QBSession currentSession;
    private static Date lastAuth;

    private QuickBloxService(Context context) {
        mContext = context;
        init(mContext);
    }

    static public QuickBloxService getInstance(Context context){
        if(mInstance == null){
            mInstance = new QuickBloxService(context);
        }
        return mInstance;
    }

    private void init(Context context){
        QBSettings.getInstance().init(context, QBConstants.APP_ID, QBConstants.AUTH_KEY, QBConstants.AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(QBConstants.ACCOUNT_KEY);
    }

    private void openSession(final Callback<QBSession, QBResponseException> callback){
        QBUser user = new QBUser("1465470797101153","123123nure");
        QBAuth.createSession(user, new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                callback.onSuccess(session);
                lastAuth = new Date();
                currentSession = session;
            }

            @Override
            public void onError(QBResponseException error) {
                callback.onFail(error);
            }
        });
    }

    private QBSession getSession(){
        if(currentSession != null && lastAuth != null){
            long diff = new Date().getTime() - lastAuth.getTime();
            if(diff > (twoH)){
                return null;
            }
            return currentSession;
        }
        return null;
    }

    public void getPlaces(final Callback<ArrayList<Place>,String> callback){
        currentSession = getSession();
        if (currentSession == null){
            openSession(new Callback<QBSession, QBResponseException>() {
                @Override
                public void onSuccess(QBSession result) {
                    gPlaces(new QBEntityCallback<ArrayList<QBCustomObject>>() {
                        @Override
                        public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                            callback.onSuccess(Place.createPlaceArray(qbCustomObjects));
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            callback.onFail(e.getLocalizedMessage());
                        }
                    });
                }

                @Override
                public void onFail(QBResponseException result) {
                    callback.onFail(result.getMessage());
                }
            });
        }
        else {
            gPlaces(new QBEntityCallback<ArrayList<QBCustomObject>>() {
                @Override
                public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                    callback.onSuccess(Place.createPlaceArray(qbCustomObjects));
                }

                @Override
                public void onError(QBResponseException e) {
                    callback.onFail(e.getLocalizedMessage());
                }
            });
        }
    }
    private void gPlaces(final QBEntityCallback<ArrayList<QBCustomObject>> callback){
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.setPagesLimit(500);

        QBCustomObjects.getObjects(Place.QB_NAME, requestBuilder, new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
                callback.onSuccess(customObjects, params);
            }

            @Override
            public void onError(QBResponseException errors) {
                callback.onError(errors);
            }
        });
    }


    public void getEvents(final String placeId, final Callback< ArrayList<Event>,String> callback) {
        currentSession = getSession();
        if (currentSession == null){
            openSession(new Callback<QBSession, QBResponseException>() {
                @Override
                public void onSuccess(QBSession result) {
                    gEvents(placeId, new QBEntityCallback<ArrayList<QBCustomObject>>() {
                        @Override
                        public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                            callback.onSuccess(Event.createEventArray(qbCustomObjects));
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            callback.onFail(e.getLocalizedMessage());
                        }
                    });
                }

                @Override
                public void onFail(QBResponseException result) {
                    callback.onFail(result.getLocalizedMessage());
                }
            });
        }
        else{
            gEvents(placeId, new QBEntityCallback<ArrayList<QBCustomObject>>() {
                @Override
                public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                    callback.onSuccess(Event.createEventArray(qbCustomObjects));
                }

                @Override
                public void onError(QBResponseException e) {
                    callback.onFail(e.getLocalizedMessage());
                }
            });
        }
    }

    private void gEvents(final String placeId, final QBEntityCallback<ArrayList<QBCustomObject>> qbEntityCallback) {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.setPagesLimit(500);
        requestBuilder.eq(Event.QB_EACH_WEEK_FIELD_NAME, true);
        requestBuilder.eq(Event.QB_PLACE_FIELD_NAME, placeId);

        QBCustomObjects.getObjects(Event.QB_OBJECT_NAME, requestBuilder, new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(final ArrayList<QBCustomObject> customObjects, Bundle params) {
                QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
                requestBuilder.setPagesLimit(500);
                requestBuilder.gt(Event.QB_DATE_FIELD_NAME, new Date().getTime());
                requestBuilder.eq(Event.QB_PLACE_FIELD_NAME, placeId);

                QBCustomObjects.getObjects(Event.QB_OBJECT_NAME, requestBuilder, new QBEntityCallback<ArrayList<QBCustomObject>>() {
                    @Override
                    public void onSuccess(ArrayList<QBCustomObject> gtCustomObjects, Bundle params) {
                        if(gtCustomObjects!=null&&gtCustomObjects.size()>0)
                            customObjects.addAll(gtCustomObjects);
                        qbEntityCallback.onSuccess(customObjects, params);
                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        qbEntityCallback.onError(errors);
                    }
                });
            }

            @Override
            public void onError(QBResponseException errors) {
                qbEntityCallback.onError(errors);
            }
        });
    }

    public void createOrder(final Order order, final Callback<Void, String> callback) {
        {
            currentSession = getSession();
            if (currentSession == null){
                openSession(new Callback<QBSession, QBResponseException>() {
                    @Override
                    public void onSuccess(QBSession result) {
                        makeOrder(order, callback);
                    }

                    @Override
                    public void onFail(QBResponseException result) {
                        callback.onFail(result.getMessage());
                    }
                });
            }
            else {
                makeOrder(order, callback);
            }
        }

    }

    public void makeOrder(final Order order, final Callback<Void, String> callback) {
        QBCustomObjects.createObject(order.getQBObject(), new QBEntityCallback<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject createdObject, Bundle params) {
                callback.onSuccess(null);
            }

            @Override
            public void onError(QBResponseException errors) {
                callback.onFail(errors.getMessage());
            }
        });
    }
}
