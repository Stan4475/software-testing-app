package com.nure.softwaretestingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.nure.softwaretestingapp.Models.Callback;
import com.nure.softwaretestingapp.Models.Place;
import com.nure.softwaretestingapp.QuickBlox.QuickBloxService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Стас on 25.05.2016.
 */
public class PlaceListActivity extends Activity {

    View splash;
    ArrayList<Place> places;
    List<String> source = new ArrayList<>();
    ListView listView;
    ArrayAdapter<String> stringArrayAdapter;
    private Button onMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_list_activity);
        splash = findViewById(R.id.splash);
        listView = (ListView) findViewById(R.id.place_list);

        onMap = (Button) findViewById(R.id.on_list);
        onMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaceListActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        QuickBloxService.getInstance(this).getPlaces(new Callback<ArrayList<Place>, String>() {

            @Override
            public void onSuccess(ArrayList<Place> result) {
                places = result;

                for(Place p : places){
                    source.add(p.getName());
                }
                stringArrayAdapter = new ArrayAdapter<String>(PlaceListActivity.this, android.R.layout.simple_list_item_1,source);
                listView.setAdapter(stringArrayAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Place clickedPl = places.get(position);
                        if (clickedPl != null) {
                            Intent eventListIntent = new Intent(PlaceListActivity.this, EventListActivity.class);
                            eventListIntent.putExtra(MapsActivity.PLACE_EXTRAS_NAME, clickedPl.getName());
                            eventListIntent.putExtra(MapsActivity.PLACE_EXTRAS_ID, clickedPl.getPlaceId());
                            startActivity(eventListIntent);
                        }
                    }
                });
                splash.setVisibility(View.INVISIBLE);
                onMap.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFail(String result) {


                AlertDialog.Builder builder = new AlertDialog.Builder(PlaceListActivity.this);
                builder.setTitle("Request failed")
                        .setMessage(result)
                        .setCancelable(false)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        PlaceListActivity.this.finish();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
    }

}
